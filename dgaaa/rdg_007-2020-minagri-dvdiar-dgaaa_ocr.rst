===================================================================
RESOLUCION Nro 007-2020-MINAGRI-DVDIAR-DGAAA
===================================================================

Ministerio de Agricultura y Riego

Resolución de Dirección General

Lima, 06 de enero de 2020

VISTO:

El Informe N* 0003-2020-MINAGRI-DVDIAR/DGAAA-DGAA-YMGL, emitido por la
Dirección de Gestión Ambiental Agraria de la Dirección General de Asuntos Ambientales
Agrarios del Ministerio de Agricultura y Riego, recaído en el expediente CUT N” 48236-2017,
sobre la solicitud de evaluación de la Declaración Ambiental de Actividades en Curso,
presentado por la empresa Hortifrut -TAL S.A.C., y;

CONSIDERANDO:
=============

Del marco legal ambiental:

  
  

Considerando 1
~~~~~~~~~~~~~~

Que, el artículo 1 de la Ley N” 28611, Ley Generali del Ambiente, señala que, “Toda
persona tiene el derecho irrenunciable a vivir en un ambiente saludable, equilibrado y adecuado
para el pleno desarrollo de la vida, y el deber de contribuir a una efectiva gestión ambiental y
de proteger el ambiente, así de sus componentes, asegurando particularmente la salud de las
personas en forma individual y colectiva, la conservación de la diversidad biológica, el
aprovechamiento sostenible de los recursos naturales y el desarrollo sostenible del país.”;


Considerando 2
~~~~~~~~~~~~~~

Que, el artículo IX de la precitada Ley, señala que, “el causante de la degradación
El del ambiente y de sus componentes, sea una persona natural o jurídica, pública o privada, está
obligada a adoptar inexcusablemente las medidas para su restauración, rehabilitación o
reparación según corresponda o, cuando lo anterior no fuera posible, a compensar en términos
ambientales los daños generados, sin perjuicio de otras responsabilidades administrativas,
civiles o penales a que hubiere lugar”;

 


Considerando 3
~~~~~~~~~~~~~~

Que, el artículo 16 de la precitada Ley, señala que, “(...) Los Instrumentos de
gestión ambiental son mecanismos orientados a la ejecución de la política ambiental, sobre la
base de los principios establecidos en la presente Ley, y en lo señalado en sus normas
complementarias y reglamentarias. (...) Constituyen medios operativos que son diseñados,
normados y aplicados con carácter funcional o complementario, para efectivizar el
cumplimiento de la Política Nacional Ambiental y las normas ambientales que rigen en el país;


Considerando 4
~~~~~~~~~~~~~~

Que, asimismo, el artículo 17 de la precitada norma, señala que, “(...) Los
instrumentos de gestión ambiental podrán ser de planificación, promoción, prevención, control,
corrección, información, financiamiento, participación, fiscalización, entre otros, rigiéndose por
sus normas legales respectivas y los principios contenidos en la presente Ley. (...) Se entiende
que constituyen instrumentos de gestión ambiental, los sistemas de gestión ambiental,
nacional, sectoriales, regionales o locales; el ordenamiento territorial ambiental; la evaluación
del impacto ambiental; los Planes de Cierre; los Planes de Contingencias; los estándares
nacionales de calidad ambiental; la certificación ambiental, las garantías ambientales; los
sistemas de información ambiental; los instrumentos económicos, la contabilidad ambiental,
estrategias, planes y programas de prevención, adecuación, control y remediación; los
mecanismos de participación ciudadana; los planes integrales de gestión de residuos; los
instrumentos orientados a conservar los recursos naturales; los instrumentos de fiscalización
ambiental y sanción; la clasificación de especies, vedas y áreas de protección y conservación;
y, en general, todos aquellos orientados al cumplimiento de los objetivos señalados en el
artículo precedente. (...) El Estado debe asegurar la coherencia y la complementariedad en el
diseño y aplicación de los instrumentos de gestión ambiental”;


Considerando 5
~~~~~~~~~~~~~~

Que, el numeral 142.2 del artículo 142 de la mencionada Ley, indica que, “Se
denomina daño ambiental a todo menoscabo material que sufre el ambiente y/o alguno de sus
componentes, que puede ser causado contraviniendo o no disposición jurídica, y que genera
efectos negativos actuales o potenciales.”;

Del marco legal específico:


Considerando 6
~~~~~~~~~~~~~~

Que, mediante Ley N” 27446, Ley del Sistema Nacional de Evaluación de Impacto
Ambiental, en su artículo 1, inc. a), se creó el Sistema Nacional de Evaluación del Impacto
Ambiental (SEIA), como “sistema único y coordinado de identificación, prevención, supervisión,
control y corrección anticipada de impactos ambientales negativos, derivados de las acciones
humanas expresadas por medio del proyecto de inversión”;


Considerando 7
~~~~~~~~~~~~~~

Que, la citada Ley en su artículo 3, modificado por artículo 1” del Decreto Legislativo
N* 1078, establece que “no podrá iniciarse la ejecución de proyectos ni actividades de
servicios y comercio referidos en el artículo 2 de la Ley del SEIA y ninguna autoridad
nacional, sectorial, regional _o local podrá aprobarlas, autorizarlas, permitirlas,
concederlas o habilitarlas si no cuentan previamente con la certificación ambiental

contenida en la Resolución expedida por la autoridad competente.” (resaltado y subrayado
nuestro);


Considerando 8
~~~~~~~~~~~~~~

Que, el artículo | del Título Preliminar de la Ley N” 28611, Ley General del
Ambiente, señala que, “Toda persona tiene el derecho irrenunciable a vivir en un ambiente
saludable, equilibrado y adecuado para el pleno desarrollo de la vida, y el deber de contribuir
a una efectiva gestión ambiental y de proteger el ambiente, así de sus componentes,
asegurando particularmente la salud de las personas en forma individual y colectiva, la
conservación de la diversidad biológica, el aprovechamiento sostenible de los recursos
=| naturales y el desarrollo sostenible del país.”, así como, el principio de sostenibilidad, regulado
en el artículo Y de la misma norma, el cual dispone que “La gestión del ambiente y de sus
componentes, así como del ejercicio y la protección de los derechos que establece la presente
Ley, se sustenta en la integración equilibrada de los aspectos sociales, ambientales y
económicos del desarrollo nacional, así como en la satisfacción de las necesidades de las
actuales y futuras generaciones”;

 


Considerando 9
~~~~~~~~~~~~~~

Que, el Reglamento de la Ley del Sistema Nacional de Evaluación del Impacto
Ambiental, aprobado mediante Decreto Supremo N” 019-2009-MINAM, establece en su
artículo 1, que: “El presente Reglamento tiene por objeto lograr la efectiva identificación,
prevención, supervisión, control y corrección anticipada de los impactos ambientales negativos
derivados de las acciones humanas expresadas por medio de proyectos de inversión, así como
de políticas, planes y programas públicos, a través del establecimiento del SEIA”;


Considerando 10
~~~~~~~~~~~~~~~

Que, el precitado Reglamento, señala en su artículo 3, entre otros principios “el
SEIA se rige por los principios establecidos en la Ley General del Ambiente”;

 

Considerando 11
~~~~~~~~~~~~~~~

Que, en la misma línea, el artículo 24, señala que “toda actividad humana que
implique construcciones, obras, servicios y otras actividades, así como las políticas, planes y
programas públicos susceptibles de causar impactos ambientales de carácter significativo, está
sujeta, de acuerdo a Ley, al Sistema Nacional de Evaluación de Impacto Ambiental - SEÍA, el
al es administrado por la Autoridad Ambiental Nacional”;

  

“Artículo 40.- La Adecuación Ambiental de Actividades en Curso

Los titulares de actividades bajo competencia y administración del Sector
Agrario que se encuentren en operación o se hayan iniciado con anterioridad a
la vigencia del presente Reglamento, deben adecuarse a las nuevas exigencias
ambientales.

Las actividades en curso serán clasificadas por la autoridad ambiental
competente del Sector Agrario, de acuerdo a lo siguiente:

 

Declaración Ambiental de Actividades en Curso (DAAC): Cuando no generen
impactos ambientales negativos significativos.

Programa de Adecuación y Manejo Ambiental (PAMA): Cuando generen
impactos ambientales negativos significativos.

El Ministerio de Agricultura desarrollará los mecanismos, criterios y formatos
para la presentación, evaluación y adecuado seguimiento de la DAAC y PAMA”;


Considerando 12
~~~~~~~~~~~~~~~

Que, además, de conformidad con lo dispuesto por el artículo 41 del referido
Reglamento, “La DAAC es el instrumento de gestión ambiental que elaboran los
titulares de actividades en curso que no generen un impacto ambiental negativo
significativo, conforme lo determine la autoridad ambiental competente del Sector
Agrario.” (resaltado nuestro);

 

Considerando 13
~~~~~~~~~~~~~~~

Que, con relación al contenido básico de la Declaración Ambiental de Actividades en
Curso, el artícuto 42 del Reglamento de Gestión Ambiental del Sector Agrario, señala lo
siguiente:

“Artículo 42- Contenido de la DAAC

El contenido de la DAAC que presente el titular de la actividad en curso, deberá
incluir como mínimo la siguiente información:

- Antecedentes.

- Marco Legal.

- Objetivo.

- Aspectos generales de la empresa.

- Descripción de la actividad.

- Diagnóstico ambiental y social del área de influencia de la actividad.

- Programa de Monitoreo Ambiental.

- Participación Ciudadana.

- Identificación y evaluación de impactos ambientales.

- Plan de Manejo Ambiental.

- Programa de Adecuación Ambiental.

- Cronograma de inversión e implementación de las medidas de manejo y

adecuación ambiental.
- Conclusiones y recomendaciones.
- Anexos”.


Considerando 14
~~~~~~~~~~~~~~~

Que, con relación al procedimiento de aprobación de la Declaración Ambiental de
Actividades en Curso, el artículo 44 del Reglamento de Gestión Ambiental del Sector Agrario,

  
     
 

e

“ Artículo 44.- El procedimiento de aprobación de la DAAC

   

A
45) 44.1 El titular de la actividad en curso presentará ante la autoridad ambiental
competente un (01) ejemplar impreso de la DAAC, debidamente suscritos por
este y por el consultor ambiental, y uno (1) en formato digital.

44.2 De requerirse opinión técnica previa de otras autoridades, ésta deberá
formularse en un plazo no mayor a veinte (20) días hábiles y hasta diez (10) días
hábiles para evaluar la subsanación de observaciones.

44.3 La autoridad ambiental competente aprobará o desaprobará la DAAC en
un plazo que no exceda de sesenta (60) días hábiles de presentado. De existir
observaciones estas deberán absolverse en un plazo máximo de veinte (20) días
hábiles siguientes de notificado el titular.

44.4 Si las observaciones planteadas al titular de la actividad materia de la
DAAC, no fueran subsanadas en su totalidad por razones sustentadas, la
autoridad ambiental competente, a solicitud de parte y por única vez, podrá
extender el plazo máximos del procedimiento confiriendo hasta veinte (20) días
hábiles adicionales, contados a partir del día siguiente del término del plazo
anteriormente concedido, para la subsanación de las observaciones
correspondientes.

44.5 Efectuada o no dicha subsanación, la DGAAA emitirá la aprobación del
instrumento de gestión ambiental respectiva de ser el caso, o declarará
denegada la solicitud, dándose por concluido el procedimiento
administrativo”. (resaltado y subrayado nuestro).

 

 


Considerando 15
~~~~~~~~~~~~~~~

Que, la Ley de Organización y Funciones del Ministerio de Agricultura y Riego,
aprobado mediante Decreto Legislativo N” 997, en su artículo 2 establece: “el Ministerio de
Agricultura y Riego es un organismo del Poder Ejecutivo”; y “tiene personería jurídica de
Derecho Público y constituye pliego presupuestal. Asimismo, en su artículo 3, señala que “El
Ministerio de Agricultura tiene por objeto diseñar, establecer, ejecutar y supervisar la Política

Nacional Agraria del Estado asumiendo la rectoría respecto a ella, de acuerdo con las
atribuciones conferidas por la Constitución Política del Perú y demás leyes. (...)”;


Considerando 16
~~~~~~~~~~~~~~~

Que, el Reglamento de Organización y Funciones del Ministerio de Agricultura y
Riego, aprobado por Decreto Supremo N” 008-2014-MINAGRI, en su artículo 64 señala que
“La Dirección General de Asuntos Ambientales, es el órgano encargado de implementar
acciones en el marco del Sistema Nacional de Gestión Ambiental para la conservación y
aprovechamiento sostenible de los recursos naturales renovables de su competencia, en
concordancia con los lineamientos de las Políticas Nacionales Agraria y Ambiental, así como
promover la gestión eficiente del recurso suelo para uso agrario”;


Considerando 17
~~~~~~~~~~~~~~~

Que, asimismo, el artículo 65 del citado Reglamento, en concordancia con el
artículo 5 del Reglamento de Gestión Ambiental del Sector Agrario aprobado mediante Decreto
Supremo N* 019-2012-AG, establece que, “...) la DGAAA es la autoridad ambiental
competente responsable de la gestión ambiental y de dirigir el proceso de evaluación ambiental
de proyectos o actividades de competencia del Sector Agrario y, aquellos relacionados con el
aprovechamiento sostenible de los recursos naturales renovables en el ámbito de su
competencia y en el marco del Sistema Nacional de Gestión Ambiental; (...)”;

 


Considerando 18
~~~~~~~~~~~~~~~

Que, de acuerdo con lo señalado en el artículo 66 y literal g) del artículo 67 del de
Organización y Funciones del Ministerio de Agricultura y Riego, la Dirección de Gestión
Ambiental Agraria (DGAA), es la unidad orgánica de la Dirección General de Asuntos
Ambientales Agrarios (DGAAA), encargada de evaluar y emitir opinión sobre los instrumentos
de gestión ambiental en el ámbito de su competencia;

De los antecedentes:


Considerando 19
~~~~~~~~~~~~~~~

Que, mediante Formulario P-8, ingresado con fecha 16 de octubre de 2017, la
empresa Hortifrut — Tal S.A.C, solicitó a la Dirección General de Asuntos Ambientales Agrarios
del Ministerio de Agricultura y Riego, la evaluación de la Declaración Ambiental de Actividades
en Curso de los “Fundos San Luis, Esperanza 1, Esperanza 2 y Esperanza 3”, ubicado en el
distrito de Chao, provincia de Virú y departamento de La Libertad;


Considerando 20
~~~~~~~~~~~~~~~

Que, mediante Oficio N” 0520-2017-DVDIAR-DGAAA-DGAA, de fecha 19 de
octubre de 2017, la Dirección de Gestión Ambiental Agraria de la Dirección General de Asuntos
Ambientales Agrarios, solicitó a la Autoridad Nacional del Agua, la opinión de la Declaración
Ambiental de Actividades en Curso para los “Fundos San Luis, Esperanza 1, Esperanza 2 y
Esperanza 3”, de titularidad de la empresa Hortifrut — Tal S.A.C.

Considerando 21
~~~~~~~~~~~~~~~

Que, mediante Oficio N” 1510-2017-ANA/DGCRH, ingresado con fecha 08 de
noviembre de 2017, la Autoridad Nacional del Agua remitió a la Dirección de Gestión Ambiental
Agraria el Informe Técnico N* 957-2017-ANA-DGCRH/EEIGA, mediante el cual formuló cuatro
(04) observaciones a la Declaración Ambiental de Actividades en Curso citada en el párrafo
precedente;


Considerando 22
~~~~~~~~~~~~~~~

Que, mediante Carta N* 0094-2018-MINAGRI-DVDIAR/DGAAA-DGAA, de fecha
13 de marzo de 2018, la Dirección General de Asuntos Ambientales Agrarios, remitió a la
empresa Hortifrut — Tal S.A.C., la Observación Técnica N” 0019-2018-MINAGRI-DVDIAR-
DGAAA-DGAA-JEBA, con treinta y tres (33) observaciones formuladas a la Declaración
Ambiental de Actividades en Curso, otorgándole veinte (20) días hábiles, contados a partir de
la fecha de notificación de dicha carta;


Considerando 23
~~~~~~~~~~~~~~~

Que, mediante Carta N* 172-ADM-2018, ingresada con fecha 16 de abril de 2018,
la empresa Hortifrut — Tal S.A.C., remitió a la Dirección General de Asuntos Ambientales
Agrarios la subsanación de las observaciones formuladas a la Declaración Ambiental de
Actividades en Curso, para los “Fundos San Luis, Esperanza 1, Esperanza 2 y Esperanza 3”;


Considerando 24
~~~~~~~~~~~~~~~

Que, mediante Oficio N” 0333-2018-MINAGRI-DVDIAR/DGAAA-DGAA, de fecha
30 de abril de 2018, la Dirección de Gestión Ambiental Agraria solicitó a la Autoridad Nacional
del Agua la opinión técnica a la subsanación de las observaciones de la Declaración Ambiental
de Actividades en Curso, para los “Fundos San Luis, Esperanza 1, Esperanza 2 y Esperanza
3, de titularidad de la empresa Hortifrut — Tal S.A.C.;

    
 
 
 


Considerando 25
~~~~~~~~~~~~~~~

Que, mediante Oficio N” 897-2018-ANA-DCERH, ingresado con fecha 11 de mayo
de 2018, la Autoridad Nacional del Agua remitió a la Dirección de Gestión Ambiental Agraria la
matriz de información complementaria N” 081-2018-ANA-DCERH-ElGA, señalando que “se
sprecisa la información requerida a complementar, que el administrado deberá presentar para
¿pmitir la opinión favorable.”;


Considerando 26
~~~~~~~~~~~~~~~

Que, mediante Carta N* 0223-2018-MINAGRI-DVDIAR/DGAAA-DGAA, de fecha
28 de mayo de 2018, la Dirección de Gestión Ambiental Agraria remitió a la empresa Hortifrut
- Tal S.A.C., la matriz de información complementaria N” 081-2018-ANA-DCERH-EIGA,
requerida por la Autoridad Nacional del Agua a través del Oficio N* 897-2018- ANA-DCERH;


Considerando 27
~~~~~~~~~~~~~~~

Que, mediante Carta N* 238-GG-2018, de fecha 19 de junio de 2018, la empresa
Hortifrut — Tal S.A.C., solicitó a la Dirección General de Asuntos Ambientales Agrarios, la
ampliación de plazo para subsanar las observaciones formuladas a la Declaración Ambiental
de Actividades en Curso para los “Fundos San Luis, Esperanza 1, Esperanza 2 y Esperanza
3;

 


Considerando 28
~~~~~~~~~~~~~~~

Que, mediante Carta N” 0301-2018-MINAGRI-DVDIAR/DGAAA-DGAA, con fecha
10 de julio de 2018, la Dirección de Gestión Ambiental Agraria concede a la empresa Hortifrut
— Tal S.A.C., diez (10) días hábiles para que absuelva las observaciones formuladas a la
Declaración Ambiental de Actividades en Curso para los “Fundos San Luis, Esperanza 1,
Esperanza 2 y Esperanza 3”,


Considerando 29
~~~~~~~~~~~~~~~

Que, mediante Carta N* 296-ADM-2018, ingresada con fecha 06 de agosto de

Me 2018, la empresa Hortifrut — Tal S.A.C., remitió la información solicitada a través de la Carta N*

se «o: “Sy 0223-2018-MINAGRI-DVDIAR/DGAAA-DGAA, requerida por la Autoridad Nacional del Agua a
¿Jitravés del Oficio N* 897-2018-ANA-DCERH;

Ade
E

   

mz Que, mediante Oficio N” 0844-2018-MINAGRI-DVDIAR/DGAAA-DGAA, de fecha
26 de octubre de 2018, la Dirección de Gestión Ambiental Agraria remitió a la Autoridad
Nacional del Agua la Información Complementaria solicitada con Oficio N” 897-2018-ANA-
DCERH, para su evaluación correspondiente;


Considerando 30
~~~~~~~~~~~~~~~

Que, mediante Carta N” 470-ADM-2018, ingresada con fecha 28 de diciembre de
2018, la empresa Hortifrut — Tal S.A.C., presentó a la Dirección General de Asuntos
Ambientales Agrarios, la Información Complementaria para ser remitida a la Autoridad Nacional
del Agua, señalando que “para un mejor resolver de la ANA y de vuestro Despacho, por medio
de la presente aclaramos y/o precisamos la información complementaria respecto de la
demanda y balance hídrico del proyecto presentada por HORTIFRUT-TAL mediante Carta N?
296:-ADM-2018; así como información relevante para la evaluación”;


Considerando 31
~~~~~~~~~~~~~~~

Que, mediante Oficio N* 0032-2019-MINAGRI-DVDIAR/DGAAA-DGAA, de fecha
z 08 de enero de 2019, la Dirección de Gestión Ambiental Agraria remitió a la Autoridad Nacional


Considerando 32
~~~~~~~~~~~~~~~

Que, mediante Oficio N* 071-2019-ANA-DCERH, ingresado con fecha 15 de enero
de 2019, la Autoridad Nacional del Agua remitió a la Dirección de Gestión Ambiental Agraria
de la Dirección General de Asuntos Ambientales Agrarios, la opinión para los “Fundos San
Luís, Esperanza 1, Esperanza 2 y Esperanza 3”;


Considerando 33
~~~~~~~~~~~~~~~

Que, mediante Carta N” 0176-2019-MINAGRI-DVDIAR/DGAAA-DGAA, de fecha
06 de febrero de 2019, la Dirección de Gestión Ambiental Agraria solicitó a la empresa Hortifrut
- Tal S.A.C., información complementaria a la subsanación de observaciones, otorgándole diez
(10) días hábiles para que presente lo solicitado;

 


Considerando 34
~~~~~~~~~~~~~~~

Que, mediante Carta N* 080-2019/ADM, ingresada con fecha 22 de febrero de
2019, la empresa Hortifrut — Tal S.A.C., solicitó a la Dirección General de Asuntos Ambientales
Agrarios, la ampliación de plazo para presentar la información complementaria para la
subsanación de las observaciones formuladas a la Declaración Ambiental de Actividades en
Curso para los “Fundos San Luis, Esperanza 1, Esperanza 2 y Esperanza 3”;


Considerando 35
~~~~~~~~~~~~~~~

Que, mediante Carta N* 0256-2019-MINAGRI-DVDIAR/DGAAA-DGAA, con fecha
28 de febrero de 2019, la Dirección de Gestión Ambiental Agraria concede a la empresa
Hortifrut — Tal S.A.C., diez (10) días hábiles para presentar la información complementaria para
la subsanación de las observaciones formuladas a la Declaración Ambiental de Actividades en
Curso para los “Fundos San Luis, Esperanza 1, Esperanza 2 y Esperanza 3";

  
  
   


Considerando 36
~~~~~~~~~~~~~~~

Que, mediante Carta N* 101-2019-ADM, ingresada con fecha 19 de marzo de 2019,
a empresa Hortifrut — Tal S.A.C., presentó a la Dirección General de Asuntos Ambientales
Agrarios la Información Complementaria de la subsanación de las observaciones de la
E Declaración Ambiental de Actividades en Curso para los “Fundos San Luis, Esperanza 1,
Esperanza 2 y Esperanza 3";

 


Considerando 37
~~~~~~~~~~~~~~~

Que, mediante Carta N” 0625-2019-MINAGRI-DVDIAR/DGAAA-DGAA, con fecha
10 de junio de 2019, la Dirección de Gestión Ambiental Agraria solicitó a la empresa Hortifrut —
Tal S.A.C., presentar la documentación inscrita en registros públicos, así como, la
documentación que sustente el inicio de las actividades agrícolas en el área de la precitada
Declaración Ambiental de Actividades en Curso;


Considerando 38
~~~~~~~~~~~~~~~

Que, mediante Carta N* 253-GG-2019, con fecha 03 de julio de 2019, la empresa
Ecofluídos Ingenierso S.A., consultora ambiental de la empresa Hortifrut — Tal S.A.C., solicitó
a la Dirección General de Asuntos Ambientales Agrarios una reunión con los especialistas a
cargo de la revisión de la Declaración Ambiental de Actividades en Curso “Fundos San Luis,
Esperanza 1, Esperanza 2 y Esperanza 3” para que se les informe la situación actual de la
evaluación de la precitada Declaración Ambiental de Actividades en Curso;


Considerando 39
~~~~~~~~~~~~~~~

Que, mediante Carta N* 296-2019-ADM, con fecha 13 de agosto de 2019, la
empresa Hortifrut — Tal S.A.C., presentó a la Dirección General de Asuntos Ambientales
Agrarios la información requerida en la Carta N* 0625-2019-MINAGRI-DVDIAR/DGAAA-
DGAA, referente a documentación que sustente el inicio de las actividades agrícolas en el área
de la Declaración Ambiental de Actividades en Curso “Fundos San Luis, Esperanza 1,
Esperanza 2 y Esperanza 3”;


Considerando 40
~~~~~~~~~~~~~~~

Que, mediante Informe Técnico N* 0340-2019-MINAGRI-DVDIAR/DGAAA-DERN-
EASP, de fecha 07 de octubre de 2019, la Dirección de Evaluación de Recursos Naturales de
la Dirección General de Asuntos Ambientales Agrarios, remitió a la Dirección de Gestión
Ambiental Agraria, el análisis cartográfico de superposición de proyecto en evaluación con las
Áreas Naturales Protegidas, concluyendo que no presenta superposición entre dichas zonas ni
con Zonas de Amortiguamiento establecidas por el Servicio Nacional de Áreas Naturales
Protegidas por el Estado;


Considerando 41
~~~~~~~~~~~~~~~

Que, mediante Carta N* 439-2019-ADM/LEG, con fecha 15 de noviembre de 2019,
+4 la empresa Hortifrut — Tal S.A.C., presentó a la Dirección General de Asuntos Ambientales
Agrarios, la solicitud de la respuesta formal referente a la DAAC “Fundos San Luis, Esperanza

1, Esperanza 2 y Esperanza 3”.

De la evaluación:


Considerando 42
~~~~~~~~~~~~~~~

Que, de la revisión de los actuados, la empresa Hortifrut — Tal S.A.C., remitió con
fecha 13 de agosto de 2019, la Carta N” 296-2019-ADM, en la cual se acreditó la titularidad
de los predios, en el predio “Reproductora 1!” el fundo San Luis, con la copia de la Partida
Electrónica N* 04033851 (folios 15 al 28'), así como, en el predio “Agonía ” los fundos
Esperanza 1, Esperanza 2 y Esperanza 3, con la copia de la Partida Electrónica N” 11064305
(folios 30 al 38?);


Considerando 43
~~~~~~~~~~~~~~~

Que, de conformidad con lo establecido en el artículo 40 del Decreto Supremo N?
019-2012-AG, modificado por Decreto Supremo N* 004-2013-AG y Decreto Supremo N* 013-
2013-MINAGRI, que los titulares de actividades bajo competencia del Sector Agrario que se
encuentren en operación o hayan iniciado con anterioridad a la vigencia del presente
Reglamento, deben adecuarse a las nuevas exigencias ambientales, y serán clasificadas por
la autoridad ambiental competente del Sector Agrario de acuerdo a lo siguiente:

Declaración Ambiental de Actividades en Curso -DAAC: cuando no generen
impactos negativos significativos.

i Carta N* 296-2019/ADM, ingresada a la DGAAA con fecha 13 de agosto de 2019.
2 (dem

e Programa de Adecuación y Manejo Ambiental — PAMA: cuando generen
impactos ambientales significativos;


Considerando 44
~~~~~~~~~~~~~~~

Que, en virtud de lo señalado precedentemente se tiene que la Declaración
Ambiental de Actividades en Curso - DAAC, constituye un instrumento de gestión ambiental
que elaboran los titulares de actividades en curso que no generen un impacto ambiental
negativo significativo, conforme lo determine la autoridad ambiental competente del Sector
Agrario;


Considerando 45
~~~~~~~~~~~~~~~

Que, ese contexto la empresa Hortifrut — Tal S.A.C., tiene que acreditar la actividad
agrícola, antes de la vigencia del Reglamento de Gestión Ambiental del Sector Agrario,
publicado en el diario Oficial El Peruano el 14 de noviembre de 2012. Ahora bien, es necesario :
precisar que se entiende: “que se encuentren en operaciór”, se debe señalar que el presente *
eglamento contempló las actividades que ya se venían ejecutando con anterioridad al 14 de
noviembre de 2012, es decir, proyectos que no se encontraban en la fase de inicio, sino en
¿ase de ejecución y/o operación;

   
    

a Que, de acuerdo a lo señalado por la empresa Hortifut — Tal S.A.C., que en el año

1995 el Proyecto Especial Chavimochic, transfirió a favor de la empresa Avícola San Luis S.A.,
el Lote Reproductora Il, posteriormente, en mayo de 2013, dicho Lote fue adquirido en mérito
a la tusión por absorción por la empresa TAL S.A., para luego éste ceder su uso y disfrute a
favor de la empresa el Rocío S.A., para el desarrollo de las actividades agrícolas (folios 2 y 3)?.
Sin embargo, se advierte que con la Consulta RUC (SUNAT) la empresa el Rocío S.A con RUC
N* 20204844381, que realiza como actividad principal la crianza de animales domésticos, con
fecha de inscripción del 12 de enero de 1994. En consecuencia, se infiere que antes de mayo
del 2013, el objetivo de dicho predio estaba orientado al fin pecuario y no al agrícola;

 


Considerando 46
~~~~~~~~~~~~~~~

Que, por otro lado, de los documentos alcanzados por la empresa Hortifrut — Tal
S.A.C., a través de la Carta N” 296-2019-ADM, se observa copia del Informe de Verificación
N* 020-2013-GRLL-PRE/PECH-03-EHDLC, de fecha 10 de setiembre de 2013 (folios 108 al
110%), elaborado por el Ingeniero, Evaristo Huanca De la Cruz, del Proyecto Especial
Chavimochic, el cual señala, entre otros, la constatación realizada con fecha 21 de mayo de
2009 a la empresa el Rocío S.A., que inició trabajos de apertura de caminos de acceso de 2,
300 metros lineales. Sin embargo, que con fecha 27 de octubre de 2010, el Proyecto Especial
Chavimochic y la empresa el Rocio S.A., suscribieron una adenda al contrato de compra venta
del Lote La Agonía 1, donde se acuerda la ampliación del plazo del compromiso de inversión
por un periodo de dos (2) años para actividades pecuarias. En consecuencia, se infiere que la

 

3 ídem
4 ídem
apertura de caminos, antes de octubre de 2010, estaba orientado al desarrollo de la actividad
pecuaria y no al agrícola;


Considerando 47
~~~~~~~~~~~~~~~

Que, asimismo, de la verificación realizada el 5 de setiembre de 2013, el Proyecto
Especial Chavimochic identificó que no se observó inversiones en el rubro de actividades
pecuarias, pero si realizó inversiones en el rubro de desarrollo físico de las tierras a través de
la nivelación gruesa, fina y subsolado del terreno de aproximadamente 70 hectáreas de las
644.45 hectáras que cuenta el predio. En consecuencia, se infiere que las actividades de
nivelación gruesa, fina y subsolado para la actividad pecuaria fueron realizadas en el año 2013,
y no antes del 14 de noviembre de 2012;


Considerando 48
~~~~~~~~~~~~~~~

Que, igualmente se advierte en el Informe de la Sexta Verificación de Inversiones
en el Lote La Agonía | El Rocío S.A., elaborado por la Subgerencia de Gestión de Tierras del
Proyecto Especial Chavimochic, que labores de nivelación gruesa y subsolado en 70 hectáreas
aproximadamente y 2 300 metros de caminos internos aproximadamente, son actividades
diferentes a las establecidas en el cronograma de inversiones que forma parte de la adenda
suscrita por el Proyecto Especial Chavimochic y la empresa el Rocío S.A., del 27 de octubre
de 2010 para actividades pecuarias (folios 119 a 121%). En consecuencia, no acredita las
inversiones para cumplir con el objetivo de la adenda cuya finalidad era la actividad pecuaria;


Considerando 49
~~~~~~~~~~~~~~~

Que, del mismo modo, en el Informe de Verificación N” 020-2013-GRLL-
PRE/PECH-03-EHDLC, de fecha 10 de setiembre del 2013, en cuyo párrafo ocho (08) de los
Antecedentes, se menciona que “La empresa El Rocío S.A. al quinto año de entrega del Lote,
no presentó documentación sustentatoria de las inversiones efectuadas en el Lote. Según
verificación, se constató que no se han realizado inversiones en el Lote “La Agonía !”
(Esperanza 1, Esperanza 2 y Esperanza 3), acorde con la carta N”* 036/2012-LT
SGDO0.833188, de fecha 19.09.2012, donde la Empresa El Rocío manifiesta que no van a
acreditar inversiones en el lote La Agonía 1”, afirmación del titular que pone en evidencia que

yo realizó actividad agraria? antes del 14 de noviembre del 2012;

   
 
 
  


Considerando 50
~~~~~~~~~~~~~~~

Que, asimismo, Hortifrut — Tal S.A.C., para el predio “Reproductora il” fundo San
Luis, señala que al citado predio se le otorgó una licencia de agua superficial con fines agrarios

expedida con la Resolucion Directoral N* 1812-2016-ANA-AAA H CH, de fecha 28 de octubre
de 2016, por la Autoridad Administrativa del Agua Huarmey-Chicama, del cual, en su artículo
primero señala que se declara la extinción de la licencia de uso de agua superficial con fines
agrarios otorgada con la Resolución Administrativa N* 011-01-DRA-LL/ATDRMVCH, a favor de
¿Avícola San Luis S.A., y en su artículo segundo señala, otorgar, licencia de uso de agua
puperticial, clase de uso productivo, tipo de uso agrario, proveniente del río Santa, a favor de

ORTIFRUT TAL S.A.C., para el predio donde se hará uso del agua en 121. 6 hectáreas (folios
$/ 406”). En consecuencia, se infiere que la actividad agrícola se pretendía desarrollar después
del otorgamiento de licencia de uso de agua superficial, es decir, a partir del 28 de octubre de
2016; Aunado a ello, mediante la copia de Hoja Resumen de la Tasación N” LAS-10528-2016,
de fecha 18 de julio de 2016, registra que en el fundo “San Luis” se realizó actividades de
mantenimiento y poda del cultivo de arándano de seis (6) meses de edad de inmueble de
terreno rústico de 121.6 hectáreas (folios 142 al 151%) documentos en los cuales, se evidencia
que no exisitia actividad en el predio materia de la solicitud, antes del 14 de noviembre del

2012;

 

   
  
    

 

57
Idem

Glosario de Términos aprobados por el Decreto Supremo N* 019-2012-AG
(...) Actividad Agraria.- Comprende la actividad agrícola, pecuaria, silvícola, la extracción de madera y de productos silvestres,
la transformación y comercialización de productos agrarios, los servicios agrarios y la asesoría técnica brindada a los productores
agrarios”. (...).

7 Carta N* 101-2019-ADM, ingresada con fecha 19 de marzo de 2019

7
Idem


Considerando 51
~~~~~~~~~~~~~~~

Que, tomando en cuenta que el inicio de actividades de la citada empresa fue con
posterioridad al 14 de noviembre de 2012, según lo descrito en los considerandos precedentes;
no es posible que se apruebe la Adecuación Ambiental de la actividad en curso, bajo los
alcances del Decreto Supremo N? 019-2012-AG, modificado por Decreto Supremo N” 004-
2013-AG y Decreto Supremo N” 013-2013-MINAGARI, mediante el cual se aprueba el
Reglamento de Gestión Ambiental del Sector Agrario, por cuanto el artículo 40 de dicho
dispositivo legal establece que los titulares de actividades bajo competencia del Sector Agrario
que se encuentren en operación o hayan iniciado con anterioridad a la vigencia del
Reglamento, deben adecuarse a las nuevas exigencias ambientales;


Considerando 52
~~~~~~~~~~~~~~~

Que, en consecuencia, si tomamos en cuenta que el indicado Decreto Supremo fue
publicado en el Diario Oficial El Peruano, el día 14 de noviembre de 2012, importa que de
acuerdo al mencionado artículo 40, la adecuación de la actividad en curso se realiza de
aquellos que a la fecha de publicación de dicho dispositivo se estén realizando o estuvieron
realizándose con anterioridad a su vigencia, lo cual no se acreditó en el presente caso;

¿ Que, sin perjuicio de lo antes anotado, el Informe del Visto señala también, que a
través de la Carta N” 0261-2018-MINAGRI-DVDIAR/DGAAA-DGAA, la Dirección de Gestión
Ambiental Agraria, formuló treinta y tres (33) observaciones a la solicitud de otorgamiento de
la Declaración Ambiental de Actividades en Curso presentada por la empresa Hortifrut — Tal
S.A.C., y que en la tramitación correspondiente, la misma ha sido merituada; sin embargo,
dos (02) de las observaciones formuladas no han sido subsanadas en su totalidad, por lo que,
de acuerdo al artículo 44, numeral 44.5 del Decreto Supremo 019-2012-AG, el cual establece
que “Efectuada o no dicha subsanación, la DGAAA emitirá la aprobación del instrumento de
gestión ambiental respectiva de ser el caso, o declarará denegada la solicitud, dándose por
concluido el procedimiento administrativo”, los mismos que se indican a continuación:

 

a) OBSERVACIÓN N” 24, remitido mediante Carta N” 0094-2018-MINAGRI-
DVDIAR/DGAAA-DGAA de fecha 13 de marzo de 2018 - “El titular de la actividad
en curso deberá presentar el Plan de Manejo de Residuos Sólidos conforme al artículo
10 del Decreto Supremo Nro. 016-2012- AG “Reglamento de Manejo de Residuos
Sólidos del Sector Agrario.”

 

a.1) Respuesta de Hortifrut — Tal S.A.C. mediante Carta N* 172-ADM-2018, con
fecha 16 de abril de 2018 — “Debido a que los almacenes de residuos sólidos
se encuentran ubicados en los fundos Avo y Armonía 4 se considera el Plan de
Manejo de Residuos Sólidos que se maneja en los respectivos fundos. En el
Anexo N*10 se adjuntan los respectivos documentos, con la firma del titular y
del profesional colegiado, representante de la consultora quién cuenta con
experiencia y especialización de en residuos sólidos.”

 
 

a.2)

a.3)

a.4)

   

Información Complementaria ingresada a la DGAA mediante Carta N” 101-
2019-ADM, con fecha 19 de marzo de 2019- “En cumplimiento del D. Leg. N*?
1278 y D.S. N* 016-2012-AG, la empresa HORTIFRUT TAL S.A.C., ha decidido
implémentar un almacén temporal de residuos no peligrosos y un almacén
temporal de residuos peligrosos dentro del área de los “Fundos San Luis,
Esperanza 1, Esperanza 2 y Esperanza 3”. Esta medida se ha incorporado a
nuestro Programa Adecuación Ambiental.”

De la evaluación realizada por la DGAA: El titular no presenta la reformulación
del Plan de Minimización y Manejo de Residuos Sólidos según lo solicitado, el
mismo que debió ser desarrollado con las consideraciones establecidas en el
Decreto Legislativo N* 1278 (articulo 52 y 53), Decreto Supremo N” 016-2012-
AG (artículo 14), y específicamente para el emplazamiento que se pretendía
certificar.

Conclusión: El Plan de Minimización y Manejo de Residuos Sólidos debió ser
presentado teniendo en cuenta las siguientes consideraciones:

Decreto Legislativo N” 1278:

Artículo 52.- Almacenamiento de residuos sólidos segregados

(..)

Las condiciones de almacenamiento de los residuos sólidos no municipales
deben estar detalladas en el IGA.

Artículo 53.- Tipos de almacenamiento de residuos sólidos no municipales

a) Almacenamiento inicial o primario: Es el almacenamiento temporal de
residuos sólidos realizado en forma inmediata en el ambiente de trabajo,
para su posterior traslado al almacenamiento intermedio o central.

Decreto Supremo N? 016-2012-AG:

Artículo 14”.- Almacenamiento temporal de residuos

El proceso de almacenamiento inicial de residuos sólidos, se realizará dentro de
las instalaciones de la actividad, teniendo en cuenta el lugar o áreas donde los
residuos sólidos se generan. Una vez acumulado, y de acuerdo a su Plan de
Manejo, el generador podrá disponer el traslado según corresponda.

b) OBSERVACIÓN N” 25, remitido mediante Carta N” 0094-2018-MINAGRI-
DVDIAR/DGAAA-DGAA de fecha 13 de marzo de 2018 - “El titular de la actividad

también deberá presentar el Plan de Contingencia conforme al artículo 12” del Decreto
Supremo N* 16-2012-AG.”

b.1)

b.2)

Respuesta de Hortifrut — Tal S.A.C. mediante Carta N* 172-ADM-2018, con
fecha 16 de abril de 2018 — “El plan de contingencia para el manejo de residuos
sólidos se encuentra incluido dentro del PMRS del fundo Avo y fundo Armonía,
adjuntado en el Anexo N*10.”

Información Complementaria ingresada a la DGAA mediante Carta N” 101-
2019-ADM, con fecha 19 de marzo de 2019- “Se ha considerado dentro del
programa de adecuación, la presentación a la autoridad competente del Plan de
Manejo de Residuos Sólidos de los “Fundos San Luis, Esperanza 1, Esperanza
2 y Esperanza 3”, y dentro del plan se describirá el plan de contingencia de
residuos sólidos.”

b.3) De la evaluación realizada por la DGAA: En la información presentada por
Hortifrut TAL S.A.C., el titular no ha desarrollado el Plan de Minimización y
Manejo de Residuos Sólidos referida a la DAAC de los “Fundos San Luis,
Esperanza 1, Esperanza 2 y Esperanza 3”.

b.4) Conclusión: En consecuencia, se determina que el Plan de Contingencia debió
ser desarrollado como parte del Contenido del Plan de Minimización y Manejo
de Residuos Sólidos, referenciado ítem 7 del artículo 10, del Reglamneto de
Manejo de los Residuos Sólidos del Sector Agrario, aprobado mediante Decreto
Supremo N* 016-2012-AG, donde se indica lo siguiente: “Determinar un Plan de
Contingencia ante un evento inesperado que genere derrame, incendio o
exposición de los residuos sólidos peligrosos y no peligrosos”.

 
  
   

28 Que, por si fuera poco, conforme se aprecia en el Informe del Visto, se realizó la

Verificación a través de las imágenes de los satélites RAPIDEYE, de fecha 06 de octubre de
“4011, LANDSAT 7, de fecha 10 de noviembre del 2012 y LANDSAT 8, de fecha 11 de abril de
52013, facilitadas por la Dirección de Evaluación de los Recursos Naturales, de la Dirección
General de Asuntos Ambientales Agrarios del Ministerio de Agricultura y Riego, donde se
aprecia que en los “Fundos San Luis, Esperanza 1, Esperanza 2 y Esperanza 3” (área materia
de evaluación) no se evidencian actividades agrícolas en curso;


Considerando 53
~~~~~~~~~~~~~~~

Que, estando al numeral 1.1 del inciso 1 del artículo IV del Texto Unico Ordenado
de la Ley N” 27444, Ley del Procedimiento Administrativo General, aprobado por Decreto
Supremo N* 004-2019-JUS; que recoge el Principio de Legalidad en el Proceso Administrativo,
el cual dispone que las autoridades administrativas deben actuar con respeto a la Constitución,
la Ley y al Derecho, dentro de las facultades que le estén atribuidas y de acuerdo con los fines
para los que les fueren conferidas; y siendo condición de procedencia de la evaluación de toda
DAAC, la acreditación de actividad agrícola antes del 14 de noviembre de 2012, y no habiendo
subsanado dos (2) de las observaciones formuladas; la solicitud de evaluación de la
Declararción Ambiental de Actividades en Curso, debe ser denegada;

 


Considerando 54
~~~~~~~~~~~~~~~

Que, estando al Informe N” 0003-2020-MINAGRI-DVDIAR/DGAAA-DGAA-YMGL,
el mismo que forma parte integrante de la presente Resolución de Dirección General, en
aplicación del numeral 6.2 del artículo 6 del TUO de la Ley N” 27444, Ley del Procedimiento
Administrativo General, aprobado mediante Decreto Supremo N” 004-2019-JUS; debidamente
visado por el Director de la Dirección de Gestión Ambiental Agraria de la Dirección General de
Asuntos Ambientales Agrarios del Ministerio de Agricultura y Riego; y,

 

De conformidad con lo dispuesto por la Ley N* 27446, Ley del Sistema Nacional de
Evaluación del Impacto Ambiental y su Reglamento, aprobado mediante Decreto Supremo
N* 019-2009-MINAM; Reglamento de Gestión Ambiental del Sector Agrario, aprobado por
Decreto Supremo N* 019-2012-AG, modificado por Decreto Supremo N* 004-2013-AG y
Decreto Supremo N* 013-2013-MINAGRI; Reglamento de Organización y Funciones del
Ministerio de Agricultura y Riego, aprobado por Decreto Supremo N* 008-2014-MINAGRI y sus
¿a.modificatorias, la Resolución Ministerial N* 157-2011-MINAM y sus modificatorias, el cual
establece los Proyectos de Inversión sujetos al Sistema Nacional de Evaluación de Impacto
Ambiental - SEIA, considerados en el anexo Il del Reglamento de la Ley N” 27446 y su
ze Aniodificatoria; y, el Texto Único Ordenado de la Ley N* 27444, Ley del Procedimiento
Administrativo General, aprobado mediante Decreto Supremo N” 004-2019-JUS;

    
  
  
 

SE RESUELVE:
============

Artículo 1
~~~~~~~~~~

Denegar la solicitud de Declaración Ambiental de Actividades en
di Curso, presentada por la empresa Hortifrut - Tal S.A.C.; por encontrarse fuera de los alcances
SF del artículo 40 del Decreto Supremo N” 019-2012-AG, que aprobó el Reglamento de Gestión

Ambiental del Sector Agrario, y sus modificatorias, conforme a los fundamentos expuestos en
la presente Resolución.

Regístrese y comuníquese.

Dirección General de Asuntos Ambientales Agrarios
